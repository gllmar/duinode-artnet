//
//  sampler.hpp
//  map_video_led
//
//  Created by gllm on 2023-02-18.
//

#ifndef sampler_hpp
#define sampler_hpp

#include <stdio.h>

#pragma once


#include "ofMain.h"
#include "ofxGui.h"
#include "ofxOsc.h"



class sampler {
public:
    void setup(int zone_id);
    void update();
    void draw();
};

#endif /* sampler_hpp */
