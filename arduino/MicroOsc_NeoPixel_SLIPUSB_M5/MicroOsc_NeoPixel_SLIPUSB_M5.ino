// MicroOsc_NeoPixel_SLIP
// by Thomas O Fredericks


// HARDWARE REQUIREMENTS
// ==================
// NeoPixel LED strip connected to pin 26
// M5atom

// REQUIRED LIBRARIES,
// ==================
// MicroOsc
// Adafruit NeoPixel

// REQUIRED CONFIGURATION
// ======================
// Set Serial baud to 250000
#define BAUDRATE 250000


#include <MicroOscSlip.h>
// The number 128 between the < > below  is the maximum number of bytes reserved for incomming messages.
// If you want to control 30 RGB NeoPixels, you need at least 90 bytes for the data (1 byte per color). We are reserving a little more (128 bytes) just in case.
// Outgoing messages are written directly to the output and do not need more reserved bytes.
// 167 pixels => 501 bytes*3 +headers = ~1600b
// 170 pixels => 510 bytes*3 +headers = ~1650b

MicroOscSlip<1650> myMicroOsc(&Serial);
MicroOscSlip<30> myMicroOscBtn(&Serial);


#include <Adafruit_NeoPixel.h>  
const int myPixelCount = 170;
const int myPixelPin = 26; //26 -> m5stack compatible pin for HEX_SK6812
const int m5PixelPin = 27; //26 -> m5stack pin for integrated led
Adafruit_NeoPixel myPixelStrip(myPixelCount, myPixelPin , NEO_RGB + NEO_KHZ800);
Adafruit_NeoPixel m5PixelStrip(1, m5PixelPin , NEO_GRB + NEO_KHZ800);

#include "M5Atom.h"


void setup() {
  // INITIATE SERIAL COMMUNICATION
  M5.begin(false, false, false); 
  Serial.begin(BAUDRATE); // dmx rate                                        
  // INITIALIZE PIXEL STRIP
  myPixelStrip.begin();
  m5PixelStrip.begin(); 
  // STARTUP ANIMATION
  // LIGHT ALL PIXELS IN WHITE THEN TURN THEM ALL OFF
  m5PixelStrip.setPixelColor(0, myPixelStrip.Color(10, 0, 0));  //  less bright
  m5PixelStrip.show();

  for ( int i=0; i < myPixelCount; i++ ) {
     //myPixelStrip.setPixelColor(i, myPixelStrip.Color(255, 255 , 255));  
     myPixelStrip.setPixelColor(i, myPixelStrip.Color(10, 10, 10));  //  less bright
     myPixelStrip.show();
     delay(10);                                       
  }
  m5PixelStrip.setPixelColor(0, myPixelStrip.Color(0, 10, 0));  //  less bright
  m5PixelStrip.show(); 
  delay(1000);
  m5PixelStrip.setPixelColor(0, myPixelStrip.Color(0, 0, 10));  //  less bright

  myPixelStrip.clear();
  myPixelStrip.show();
  m5PixelStrip.clear();
  m5PixelStrip.show();
}

    
// FUNCTION THAT IS CALLED FOR EVERY RECEIVED OSC MESSAGE
void myOnReceiveMessage( MicroOscMessage& receivedOscMessage ) {
  
  
  // IF THE ADDRESS IS /rgb
  if ( receivedOscMessage.fullMatch("/rgb") ) {   
                 
    // CREATE A VARIABLE TO STORE THE POINTER TO THE DATA
    const uint8_t* blobData;                                    
    // GET THE DATA SIZE AND THE POINTER TO THE DATA
    int blobSize = receivedOscMessage.nextAsBlob(&blobData);
//    myMicroOsc.sendFloat( "/test/blobsize", blobSize);   
    // IF DATA SIZE IS LARGER THAN 0 AND IS A MULTIPLE OF 3 AS REQUIRED BY RGB PIXELS
    if ( blobSize > 0 && blobSize % 3 == 0 ) {                  
      // DIVIDE THE DATA BY 3 TO GET THE NUMBER OF PIXELS
      int blobPixelCount = blobSize / 3;                        
      // ITERATE THROUGH EACH PIXEL IN THE BLOB 
      for ( int i = 0 ; i < blobPixelCount ; i++ ) {            
        // EACH PIXEL HAS 3 BYTES, SO WE GO THROUGH THE DATA, 3 AT A TIME
        int blobIndex = i * 3; 
        uint8_t red =  myPixelStrip.gamma8(blobData[blobIndex]);
        uint8_t green =  myPixelStrip.gamma8(blobData[blobIndex+1]); 
        uint8_t blue =  myPixelStrip.gamma8(blobData[blobIndex+2]);                            
        myPixelStrip.setPixelColor(i, myPixelStrip.Color(red , green , blue));
      }
      uint8_t red =  myPixelStrip.gamma8(blobData[0]);
      uint8_t green =  myPixelStrip.gamma8(blobData[0+1]); 
      uint8_t blue =  myPixelStrip.gamma8(blobData[0+2]);          
      m5PixelStrip.setPixelColor(0, m5PixelStrip.Color(red , green , blue));
      m5PixelStrip.show();
      myPixelStrip.show();
    }
  }
}

void loop() {
  myMicroOsc.receiveMessages( myOnReceiveMessage );
  M5.update();  
  if (M5.Btn.wasPressed()) { myMicroOscBtn.sendInt( "/btn" ,1 );}
  if (M5.Btn.wasReleased()) { myMicroOscBtn.sendInt( "/btn" ,0 );}

}
