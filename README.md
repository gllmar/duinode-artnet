# Duinode Artnet

## Arduino code

dep : microosc, m5stack 

## slipled server

dep : puredata, OLA

### build OLA


```

sudo apt-get install libcppunit-dev libcppunit-1.15-0 uuid-dev pkg-config libncurses5-dev libtool autoconf automake g++ libmicrohttpd-dev libmicrohttpd12 protobuf-compiler libprotobuf-lite23 libprotobuf-dev libprotoc-dev zlib1g-dev bison flex make libftdi-dev libftdi1 libusb-1.0-0-dev liblo-dev libavahi-client-dev
 
pip install numpy protobuf

cd ~/src
git clone https://github.com/OpenLightingProject/ola.git ola
cd ola
autoreconf -i
./configure --enable-rdm-tests
make -j4
make check
sudo make install
sudo ldconfig

```
### OLA 

#### PORT 
9090

#### CONF
/home/pi/.ola/ola-osc.conf

```
enabled = true
input_ports = 5
output_ports = 5
port_0_address = /dmx/universe/%d
port_0_output_format = blob
port_0_targets = 127.0.0.1:50000/dmx/%d
port_1_address = /dmx/universe/%d
port_1_output_format = blob
port_1_targets = 127.0.0.1:50001
port_2_address = /dmx/universe/%d
port_2_output_format = blob
port_2_targets = 127.0.0.1:50002
port_3_address = /dmx/universe/%d
port_3_output_format = blob
port_3_targets = 127.0.0.1:50003
port_4_address = /dmx/universe/%d
port_4_output_format = blob
port_4_targets = 127.0.0.1:50004
udp_listen_port = 7770
```

## Description

## Visuals

## Installation

## Usage

## Support

## Roadmap

## Contributing

## Authors and acknowledgment

## License

## Project status

